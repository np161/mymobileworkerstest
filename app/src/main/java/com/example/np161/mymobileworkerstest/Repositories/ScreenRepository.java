package com.example.np161.mymobileworkerstest.Repositories;

import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.np161.mymobileworkerstest.Core.MMWTest;
import com.example.np161.mymobileworkerstest.Database.Daos.ScreenDao;
import com.example.np161.mymobileworkerstest.Entities.Screen;

import java.util.List;

public class ScreenRepository {

    private ScreenDao screenDao;
    private LiveData<List<Screen>> screenList;

    public ScreenRepository(int jobId){
        screenDao = MMWTest.getDatabase().screenDao();
        screenList = screenDao.getScreenList(jobId);
    }

    public void insert(Screen screen){ new InsertScreenTask(screenDao).execute(screen); }

    public void update(Screen screen){ new UpdateScreenTask(screenDao).execute(screen); }

    public void delete(Screen screen){ new DeleteScreenTask(screenDao).execute(screen); }

    public LiveData<List<Screen>> getScreenList() {
        return screenList;
    }


    private static class InsertScreenTask extends AsyncTask<Screen, Void, Void> {
        private ScreenDao screenDao;

        private InsertScreenTask(ScreenDao screenDao){
            this.screenDao = screenDao;
        }

        @Override
        protected Void doInBackground(Screen... screens) {
            screenDao.insert(screens[0]);
            return null;
        }
    }

    private static class UpdateScreenTask extends AsyncTask<Screen, Void, Void>{
        private ScreenDao screenDao;

        private UpdateScreenTask(ScreenDao screenDao){
            this.screenDao = screenDao;
        }

        @Override
        protected Void doInBackground(Screen... screens) {
            screenDao.update(screens[0]);
            return null;
        }
    }

    private static class DeleteScreenTask extends AsyncTask<Screen, Void, Void>{
        private ScreenDao screenDao;

        private DeleteScreenTask(ScreenDao screenDao){
            this.screenDao = screenDao;
        }

        @Override
        protected Void doInBackground(Screen... screens) {
            screenDao.delete(screens[0]);
            return null;
        }
    }
}
