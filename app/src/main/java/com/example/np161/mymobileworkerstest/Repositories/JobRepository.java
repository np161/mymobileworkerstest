package com.example.np161.mymobileworkerstest.Repositories;

import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.np161.mymobileworkerstest.Core.MMWTest;
import com.example.np161.mymobileworkerstest.Database.Daos.JobDao;
import com.example.np161.mymobileworkerstest.Entities.Job;

import java.util.List;

public class JobRepository {

    private JobDao jobDao;
    private LiveData<List<Job>> jobList;

    public JobRepository(){
        jobDao = MMWTest.getDatabase().jobDao();
        jobList = jobDao.getJobList();
    }

    public void insert(Job job){
        new InsertJobTask(jobDao).execute(job);
    }

    public void update(Job job){
        new UpdateJobTask(jobDao).execute(job);
    }

    public void delete(Job job){
        new DeleteJobTask(jobDao).execute(job);
    }

    public LiveData<List<Job>> getJobList() {
        return jobList;
    }


    private static class InsertJobTask extends AsyncTask<Job, Void, Void>{
        private JobDao jobDao;

        private InsertJobTask(JobDao jobDao){
            this.jobDao = jobDao;
        }

        @Override
        protected Void doInBackground(Job... jobs) {
            jobDao.insert(jobs[0]);
            return null;
        }
    }

    private static class UpdateJobTask extends AsyncTask<Job, Void, Void>{
        private JobDao jobDao;

        private UpdateJobTask(JobDao jobDao){
            this.jobDao = jobDao;
        }

        @Override
        protected Void doInBackground(Job... jobs) {
            jobDao.update(jobs[0]);
            return null;
        }
    }

    private static class DeleteJobTask extends AsyncTask<Job, Void, Void>{
        private JobDao jobDao;

        private DeleteJobTask(JobDao jobDao){
            this.jobDao = jobDao;
        }

        @Override
        protected Void doInBackground(Job... jobs) {
            jobDao.delete(jobs[0]);
            return null;
        }
    }
}
