package com.example.np161.mymobileworkerstest.Repositories;

import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.np161.mymobileworkerstest.Core.MMWTest;
import com.example.np161.mymobileworkerstest.Database.Daos.JobPhotoDao;
import com.example.np161.mymobileworkerstest.Entities.JobPhoto;

import java.util.List;

public class JobPhotoRepository {

    private JobPhotoDao jobPhotoDao;
    private LiveData<List<JobPhoto>> jobPhotos;

    public JobPhotoRepository(int photosListId){
        jobPhotoDao = MMWTest.getDatabase().jobPhotoDao();
        jobPhotos = jobPhotoDao.getJobPhotoForPhotosList(photosListId);
    }

    public void insert(JobPhoto jobPhoto){ new InsertJobPhotoTask(jobPhotoDao).execute(jobPhoto); }

    public void update(JobPhoto jobPhoto){ new UpdateJobPhotoTask(jobPhotoDao).execute(jobPhoto); }

    public void delete(JobPhoto jobPhoto){ new DeleteJobPhotoTask(jobPhotoDao).execute(jobPhoto); }

    public LiveData<List<JobPhoto>> getJobPhotos() {
        return jobPhotos;
    }


    private static class InsertJobPhotoTask extends AsyncTask<JobPhoto, Void, Void> {
        private JobPhotoDao jobPhotoDao;

        private InsertJobPhotoTask(JobPhotoDao jobPhotoDao){
            this.jobPhotoDao = jobPhotoDao;
        }

        @Override
        protected Void doInBackground(JobPhoto... jobPhotos) {
            jobPhotoDao.insert(jobPhotos[0]);
            return null;
        }
    }

    private static class UpdateJobPhotoTask extends AsyncTask<JobPhoto, Void, Void>{
        private JobPhotoDao jobPhotoDao;

        private UpdateJobPhotoTask(JobPhotoDao jobPhotoDao){
            this.jobPhotoDao = jobPhotoDao;
        }

        @Override
        protected Void doInBackground(JobPhoto... jobPhotos) {
            jobPhotoDao.update(jobPhotos[0]);
            return null;
        }
    }

    private static class DeleteJobPhotoTask extends AsyncTask<JobPhoto, Void, Void>{
        private JobPhotoDao jobPhotoDao;

        private DeleteJobPhotoTask(JobPhotoDao jobPhotoDao){
            this.jobPhotoDao = jobPhotoDao;
        }

        @Override
        protected Void doInBackground(JobPhoto... jobPhotos) {
            jobPhotoDao.delete(jobPhotos[0]);
            return null;
        }
    }
}
