package com.example.np161.mymobileworkerstest.ViewModels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.np161.mymobileworkerstest.Entities.PhotosList;
import com.example.np161.mymobileworkerstest.Repositories.PhotosListRepository;

import java.util.List;

public class PhotosListViewModel extends AndroidViewModel {
    private PhotosListRepository photosListRepository;
    private LiveData<List<PhotosList>> photosList;

    public PhotosListViewModel(@NonNull Application application, int screenId) {
        super(application);

        photosListRepository = new PhotosListRepository(screenId);
        photosList = photosListRepository.getPhotosListList();
    }

    public void insert(PhotosList photosList){
        photosListRepository.insert(photosList);
    }

    public void update(PhotosList photosList){
        photosListRepository.update(photosList);
    }

    public void delete(PhotosList photosList){
        photosListRepository.delete(photosList);
    }

    public LiveData<List<PhotosList>> getPhotosListList(){
        return photosList;
    }
}