package com.example.np161.mymobileworkerstest.Retrofit;

import com.example.np161.mymobileworkerstest.Entities.ContainerObject;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface RetrofitInterface {

    @GET("test")    Observable<ContainerObject> getJobs();
}
