package com.example.np161.mymobileworkerstest.ViewModels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.np161.mymobileworkerstest.Entities.Job;
import com.example.np161.mymobileworkerstest.Repositories.JobRepository;

import java.util.List;

public class JobViewModel extends AndroidViewModel {
    private JobRepository jobRepository;
    private LiveData<List<Job>> jobList;

    public JobViewModel(@NonNull Application application) {
        super(application);

        jobRepository = new JobRepository();
        jobList = jobRepository.getJobList();
    }

    public void insert(Job job){
        jobRepository.insert(job);
    }

    public void update(Job job){
        jobRepository.update(job);
    }

    public void delete(Job job){
        jobRepository.delete(job);
    }

    public LiveData<List<Job>> getJobList(){
        return jobList;
    }
}
