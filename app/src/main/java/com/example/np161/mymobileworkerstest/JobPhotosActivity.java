package com.example.np161.mymobileworkerstest;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.np161.mymobileworkerstest.Adpaters.JobPhotoListAdapter;
import com.example.np161.mymobileworkerstest.Core.AsyncTaskHelper;
import com.example.np161.mymobileworkerstest.Core.MMWTest;
import com.example.np161.mymobileworkerstest.Entities.JobPhoto;
import com.example.np161.mymobileworkerstest.ViewModels.Factories.JobPhotoViewModelFactory;
import com.example.np161.mymobileworkerstest.ViewModels.JobPhotoViewModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import gun0912.tedbottompicker.TedBottomPicker;

public class JobPhotosActivity extends AppCompatActivity {

    @BindView(R.id.job_number_tview) TextView job_number;
    @BindView(R.id.add_photo_btn) Button addPhotoBtn;
    @BindView(R.id.job_photo_list) RecyclerView jobPhotoList;

    private JobPhotoViewModel jobPhotoViewModel;

    private int photosListId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_photos);

        ButterKnife.bind(this);

        jobPhotoList.setLayoutManager(new LinearLayoutManager(this));
        jobPhotoList.setHasFixedSize(true);

        photosListId = getIntent().getExtras().getInt("photosListId");
        job_number.setText(MMWTest.getJob().getJobNumber() + " " + getIntent().getExtras().getString("photosListType") + " ");

        final JobPhotoListAdapter adapter = new JobPhotoListAdapter();
        jobPhotoList.setAdapter(adapter);

        jobPhotoViewModel = ViewModelProviders.of(this, new JobPhotoViewModelFactory(this.getApplication(), photosListId)).get(JobPhotoViewModel.class);
        jobPhotoViewModel.getJobPhotoList().observe(this, new android.arch.lifecycle.Observer<List<JobPhoto>>() {
            @Override
            public void onChanged(@Nullable List<JobPhoto> jobPhotos) {
                adapter.setJobPhotos(jobPhotos);
            }
        });


        addPhotoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = null;

                try {
                    fragmentManager = getSupportFragmentManager();
                } catch (ClassCastException e) {
                    Log.d("FRAG MANAGER", e.toString());
                }

                TedBottomPicker bottomSheetDialogFragment = new TedBottomPicker.Builder(getApplicationContext())
                        .setOnImageSelectedListener(new TedBottomPicker.OnImageSelectedListener() {
                            float latitude;
                            float longitude;

                            @Override
                            public void onImageSelected(Uri uri) {
                                if (ActivityCompat.checkSelfPermission(JobPhotosActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(JobPhotosActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                                    LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                                    Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                                    longitude = (float) location.getLongitude();
                                    latitude = (float) location.getLatitude();
                                }

                                new AsyncTaskHelper.savePhotos(JobPhotosActivity.this, uri, latitude, longitude, photosListId).execute();
                            }
                        })
                        .setCompleteButtonText("Done")
                        .setEmptySelectionText("No Photo Selected")
                        .create();

                bottomSheetDialogFragment.show(fragmentManager);
            }
        });
    }
}
