package com.example.np161.mymobileworkerstest;

import android.Manifest;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.np161.mymobileworkerstest.Adpaters.JobListAdapter;
import com.example.np161.mymobileworkerstest.Core.AsyncTaskHelper;
import com.example.np161.mymobileworkerstest.Core.MMWTest;
import com.example.np161.mymobileworkerstest.Entities.ContainerObject;
import com.example.np161.mymobileworkerstest.Entities.Job;
import com.example.np161.mymobileworkerstest.Core.DialogHelper;
import com.example.np161.mymobileworkerstest.Utils.NetworkHelper;
import com.example.np161.mymobileworkerstest.ViewModels.JobViewModel;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.btn_get_jobs)    Button          getJobsBtn;
    @BindView(R.id.job_list)        RecyclerView    jobList;

    private JobViewModel jobViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        jobList.setLayoutManager(new LinearLayoutManager(this));
        jobList.setHasFixedSize(true);

        final JobListAdapter adapter = new JobListAdapter();
        jobList.setAdapter(adapter);

        jobViewModel = ViewModelProviders.of(this).get(JobViewModel.class);
        jobViewModel.getJobList().observe(this, new android.arch.lifecycle.Observer<List<Job>>() {
            @Override
            public void onChanged(@Nullable List<Job> jobs) {
                adapter.setJobs(jobs);
            }
        });

        getJobsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getJobs();
            }
        });


        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() { }

            @Override
            public void onPermissionDenied(List<String> deniedPermissions) {
                Toast.makeText(MainActivity.this, "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
            }
        };

        TedPermission.with(this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage("If you reject permission, you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(
                        Manifest.permission.INTERNET,
                        Manifest.permission.ACCESS_NETWORK_STATE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION
                )
                .check();
    }

    public void getJobs(){
        if(NetworkHelper.isConnectedWifi(this)){
            MMWTest.getRetrofit().getJobs().subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<ContainerObject>() {
                        @Override
                        public void onSubscribe(Disposable d) { }

                        @Override
                        public void onNext(ContainerObject result) {
                            if (result != null) new AsyncTaskHelper.saveData().execute(result);
                        }

                        @Override
                        public void onError(Throwable e) {
                            DialogHelper.showSwalDialog(MainActivity.this, "DATA ERROR", e.getMessage(), SweetAlertDialog.ERROR_TYPE);
                        }

                        @Override
                        public void onComplete() { }
                    });
        } else {
            DialogHelper.showSwalDialog(this, "NO NETWORK CONNECTION", "Please connect to a wifi connection to download jobs", SweetAlertDialog.ERROR_TYPE);
        }
    }
}
