package com.example.np161.mymobileworkerstest.Adpaters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.np161.mymobileworkerstest.Core.MMWTest;
import com.example.np161.mymobileworkerstest.Entities.Job;
import com.example.np161.mymobileworkerstest.JobScreensActivity;
import com.example.np161.mymobileworkerstest.R;

import java.util.ArrayList;
import java.util.List;

public class JobListAdapter extends RecyclerView.Adapter<JobListAdapter.JobHolder>{
    private List<Job> jobs = new ArrayList<>();
    private Context context;

    @NonNull
    @Override
    public JobHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_job, viewGroup, false);
        context = viewGroup.getContext();
        return new JobHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull JobHolder jobHolder, int i) {
        final Job job = jobs.get(i);
        jobHolder.jobNumber.setText( job.getJobNumber() );
        jobHolder.viewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MMWTest.setJob(job);
                Intent i = new Intent(context, JobScreensActivity.class);
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return jobs.size();
    }

    public void setJobs(List<Job> jobs){
        this.jobs = jobs;
        notifyDataSetChanged();
    }

    class JobHolder extends RecyclerView.ViewHolder{
        private TextView jobNumber;
        private TextView viewBtn;


        private JobHolder(@NonNull View itemView) {
            super(itemView);

            jobNumber = itemView.findViewById(R.id.job_number_tview);
            viewBtn = itemView.findViewById(R.id.view_screens_btn);
        }
    }
}
