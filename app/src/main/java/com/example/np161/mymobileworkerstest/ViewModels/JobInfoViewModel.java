package com.example.np161.mymobileworkerstest.ViewModels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.np161.mymobileworkerstest.Entities.JobInfo;
import com.example.np161.mymobileworkerstest.Repositories.JobInfoRepository;

public class JobInfoViewModel extends AndroidViewModel {
    private JobInfoRepository jobInfoRepository;
    private LiveData<JobInfo> jobInfo;

    public JobInfoViewModel(@NonNull Application application, int screenId) {
        super(application);

        jobInfoRepository = new JobInfoRepository(screenId);
        jobInfo = jobInfoRepository.getJobInfo();
    }

    public void insert(JobInfo jobInfo){
        jobInfoRepository.insert(jobInfo);
    }

    public void update(JobInfo jobInfo){
        jobInfoRepository.update(jobInfo);
    }

    public void delete(JobInfo jobInfo){
        jobInfoRepository.delete(jobInfo);
    }

    public LiveData<JobInfo> getJobInfo(){
        return jobInfo;
    }
}
