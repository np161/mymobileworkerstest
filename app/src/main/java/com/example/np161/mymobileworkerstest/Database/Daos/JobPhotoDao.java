package com.example.np161.mymobileworkerstest.Database.Daos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.np161.mymobileworkerstest.Entities.JobPhoto;

import java.util.List;

@Dao
public interface JobPhotoDao {

    @Query(" SELECT * FROM photos where photos.id = :jobPhotoId")
    JobPhoto getJobPhoto(int jobPhotoId);

    @Query("SELECT * FROM photos where photos.photos_list_id = :photosListId")
    LiveData<List<JobPhoto>> getJobPhotoForPhotosList(int photosListId);

    @Insert()
    long insert(JobPhoto jobPhoto);

    @Update()
    void update(JobPhoto jobPhoto);

    @Delete
    void delete(JobPhoto jobPhoto);
}
