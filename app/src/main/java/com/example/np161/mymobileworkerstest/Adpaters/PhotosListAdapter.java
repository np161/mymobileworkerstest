package com.example.np161.mymobileworkerstest.Adpaters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.np161.mymobileworkerstest.Entities.PhotosList;
import com.example.np161.mymobileworkerstest.JobPhotosActivity;
import com.example.np161.mymobileworkerstest.R;

import java.util.ArrayList;
import java.util.List;

public class PhotosListAdapter extends RecyclerView.Adapter<PhotosListAdapter.PhotosListHolder> {
    private List<PhotosList> photosLists = new ArrayList<>();
    private Context context;

    @NonNull
    @Override
    public PhotosListAdapter.PhotosListHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_photos_list, viewGroup, false);
        context = viewGroup.getContext();
        return new PhotosListAdapter.PhotosListHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PhotosListAdapter.PhotosListHolder photosListHolder, int i) {
        final PhotosList photosList = photosLists.get(i);

        photosListHolder.type.setText(photosList.getPhotoType());

        if (photosList.getMinPhotosRequired() != null) {
            photosListHolder.minRequired.setText(photosList.getMinPhotosRequired() + "");
        } else {
            photosListHolder.minRequired.setText("0");
        }

        if(photosList.getAnnotationRequired()){
            photosListHolder.annotation.setText("REQUIRED");
        } else {
            photosListHolder.annotation.setText("NOT REQUIRED");
        }

        photosListHolder.viewPhotosbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent i = new Intent(context, JobPhotosActivity.class)
                       .putExtra("photosListId", photosList.getId())
                       .putExtra("photosListType", photosList.getPhotoType());
               context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return photosLists.size();
    }

    public void setPhotosLists(List<PhotosList> photosLists){
        this.photosLists = photosLists;
        notifyDataSetChanged();
    }

    class PhotosListHolder extends RecyclerView.ViewHolder{
        private TextView type;
        private TextView minRequired;
        private TextView annotation;
        private TextView viewPhotosbtn;

        private PhotosListHolder(@NonNull View itemView) {
            super(itemView);

            type = itemView.findViewById(R.id.type_tview);
            minRequired = itemView.findViewById(R.id.min_required_tview);
            annotation = itemView.findViewById(R.id.annotation_tview);
            viewPhotosbtn = itemView.findViewById(R.id.view_photos_btn);
        }
    }
}