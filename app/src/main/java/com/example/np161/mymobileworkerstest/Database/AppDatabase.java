package com.example.np161.mymobileworkerstest.Database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.np161.mymobileworkerstest.Database.Daos.JobDao;
import com.example.np161.mymobileworkerstest.Database.Daos.JobInfoDao;
import com.example.np161.mymobileworkerstest.Database.Daos.JobPhotoDao;
import com.example.np161.mymobileworkerstest.Database.Daos.PhotosListDao;
import com.example.np161.mymobileworkerstest.Database.Daos.ScreenDao;
import com.example.np161.mymobileworkerstest.Entities.Job;
import com.example.np161.mymobileworkerstest.Entities.JobInfo;
import com.example.np161.mymobileworkerstest.Entities.JobPhoto;
import com.example.np161.mymobileworkerstest.Entities.PhotosList;
import com.example.np161.mymobileworkerstest.Entities.Screen;

@Database(entities = {
        Job.class,
        Screen.class,
        JobInfo.class,
        PhotosList.class,
        JobPhoto.class
}, version = 1)

public abstract class AppDatabase extends RoomDatabase {

    public abstract JobDao jobDao();
    public abstract ScreenDao screenDao();
    public abstract JobInfoDao jobInfoDao();
    public abstract PhotosListDao photosListDao();
    public abstract JobPhotoDao jobPhotoDao();
}


