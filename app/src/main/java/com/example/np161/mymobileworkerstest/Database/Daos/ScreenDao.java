package com.example.np161.mymobileworkerstest.Database.Daos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.np161.mymobileworkerstest.Entities.Screen;

import java.util.List;

@Dao
public interface ScreenDao {

    @Query("SELECT * FROM screens where screens.job_id = :jobId")
    LiveData<List<Screen>> getScreenList(int jobId);

    @Insert()
    long insert(Screen screen);

    @Update()
    void update(Screen screen);

    @Delete
    void delete(Screen screen);
}
