package com.example.np161.mymobileworkerstest;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.example.np161.mymobileworkerstest.Adpaters.ScreenListAdapter;
import com.example.np161.mymobileworkerstest.Core.MMWTest;
import com.example.np161.mymobileworkerstest.Entities.Screen;
import com.example.np161.mymobileworkerstest.ViewModels.ScreenViewModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class JobScreensActivity extends AppCompatActivity {

    @BindView(R.id.job_number_tview) TextView job_number;
    @BindView(R.id.screen_list) RecyclerView screenList;

    private ScreenViewModel screenViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_screens);

        ButterKnife.bind(this);

        screenList.setLayoutManager(new LinearLayoutManager(this));
        screenList.setHasFixedSize(true);

        job_number.setText(MMWTest.getJob().getJobNumber() + " ");

        final ScreenListAdapter adapter = new ScreenListAdapter();
        screenList.setAdapter(adapter);

        screenViewModel = ViewModelProviders.of(this).get(ScreenViewModel.class);
        screenViewModel.getScreenList().observe(this, new android.arch.lifecycle.Observer<List<Screen>>() {
            @Override
            public void onChanged(@Nullable List<Screen> screens) {
                adapter.setScreens(screens);
            }
        });

    }
}
