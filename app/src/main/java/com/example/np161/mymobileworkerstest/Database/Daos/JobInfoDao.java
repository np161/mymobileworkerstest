package com.example.np161.mymobileworkerstest.Database.Daos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.np161.mymobileworkerstest.Entities.JobInfo;

@Dao
public interface JobInfoDao {

    @Query("SELECT * FROM job_infos where job_infos.screen_id = :screenId")
    LiveData<JobInfo> getJobInfoByScreen(int screenId);

    @Insert
    void insert(JobInfo jobInfo);

    @Update
    void update (JobInfo jobInfo);

    @Delete
    void delete(JobInfo jobInfo);
}
