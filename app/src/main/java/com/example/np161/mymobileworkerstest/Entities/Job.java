package com.example.np161.mymobileworkerstest.Entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@Entity(tableName = "jobs")
public class Job {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "job_number")
    @SerializedName("job_number")
    @Expose
    private String jobNumber;

    @Ignore
    @SerializedName("screens")
    @Expose
    private List<Screen> screens = null;

    public Job(int id, String jobNumber) {
        this.id = id;
        this.jobNumber = jobNumber;
    }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public String getJobNumber() {
        return jobNumber;
    }

    public void setJobNumber(String jobNumber) {
        this.jobNumber = jobNumber;
    }

    public List<Screen> getScreens() { return screens; }

    public void setScreens(List<Screen> screens) {
        this.screens = screens;
    }
}
