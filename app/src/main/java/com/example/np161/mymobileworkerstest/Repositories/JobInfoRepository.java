package com.example.np161.mymobileworkerstest.Repositories;

import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.np161.mymobileworkerstest.Core.MMWTest;
import com.example.np161.mymobileworkerstest.Database.Daos.JobInfoDao;
import com.example.np161.mymobileworkerstest.Entities.JobInfo;

public class JobInfoRepository {

    private JobInfoDao jobInfoDao;
    private LiveData<JobInfo> jobInfo;

    public JobInfoRepository(int screenId){
        jobInfoDao = MMWTest.getDatabase().jobInfoDao();
        jobInfo = jobInfoDao.getJobInfoByScreen(screenId);
    }

    public void insert(JobInfo jobInfo){ new InsertJobInfoTask(jobInfoDao).execute(jobInfo); }

    public void update(JobInfo jobInfo){ new UpdateJobInfoTask(jobInfoDao).execute(jobInfo); }

    public void delete(JobInfo jobInfo){ new DeleteJobInfoTask(jobInfoDao).execute(jobInfo); }

    public LiveData<JobInfo> getJobInfo() {
        return jobInfo;
    }


    private static class InsertJobInfoTask extends AsyncTask<JobInfo, Void, Void> {
        private JobInfoDao jobInfoDao;

        private InsertJobInfoTask(JobInfoDao jobInfoDao){
            this.jobInfoDao = jobInfoDao;
        }

        @Override
        protected Void doInBackground(JobInfo... jobInfos) {
            jobInfoDao.insert(jobInfos[0]);
            return null;
        }
    }

    private static class UpdateJobInfoTask extends AsyncTask<JobInfo, Void, Void>{
        private JobInfoDao jobInfoDao;

        private UpdateJobInfoTask(JobInfoDao jobInfoDao){
            this.jobInfoDao = jobInfoDao;
        }

        @Override
        protected Void doInBackground(JobInfo... jobInfos) {
            jobInfoDao.update(jobInfos[0]);
            return null;
        }
    }

    private static class DeleteJobInfoTask extends AsyncTask<JobInfo, Void, Void>{
        private JobInfoDao jobInfoDao;

        private DeleteJobInfoTask(JobInfoDao jobInfoDao){
            this.jobInfoDao = jobInfoDao;
        }

        @Override
        protected Void doInBackground(JobInfo... jobInfos) {
            jobInfoDao.delete(jobInfos[0]);
            return null;
        }
    }
}
