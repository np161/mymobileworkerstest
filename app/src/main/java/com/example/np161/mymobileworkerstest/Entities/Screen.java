package com.example.np161.mymobileworkerstest.Entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@Entity(tableName = "screens", foreignKeys = {@ForeignKey(entity = Job.class, parentColumns = "id", childColumns = "job_id", onDelete = ForeignKey.CASCADE)})
public class Screen {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "screen_name")
    @SerializedName("screen_name")
    @Expose
    private String screenName;

    @Ignore
    @SerializedName("job_info")
    @Expose
    private JobInfo jobInfo;

    @Ignore
    @SerializedName("photos_list")
    @Expose
    private List<PhotosList> photosList = null;

    @ColumnInfo(name = "job_id")
    private int jobId;

    public Screen(int id, String screenName, int jobId) {
        this.id = id;
        this.screenName = screenName;
        this.jobId = jobId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public JobInfo getJobInfo() {
        return jobInfo;
    }

    public void setJobInfo(JobInfo jobInfo) {
        this.jobInfo = jobInfo;
    }

    public List<PhotosList> getPhotosList() {
        return photosList;
    }

    public void setPhotosList(List<PhotosList> photosList) {
        this.photosList = photosList;
    }

    public int getJobId() {
        return jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }
}
