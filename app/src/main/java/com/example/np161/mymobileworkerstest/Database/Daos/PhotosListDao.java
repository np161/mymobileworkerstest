package com.example.np161.mymobileworkerstest.Database.Daos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.np161.mymobileworkerstest.Entities.PhotosList;

import java.util.List;

@Dao
public interface PhotosListDao {

    @Query(" SELECT * FROM photos_lists where photos_lists.screen_id = :screenId")
    LiveData<List<PhotosList>> getPhotosListForScreen(int screenId);

    @Insert
    void insert(PhotosList photosList);

    @Update
    void update(PhotosList photosList);

    @Delete
    void delete(PhotosList photosList);
}
