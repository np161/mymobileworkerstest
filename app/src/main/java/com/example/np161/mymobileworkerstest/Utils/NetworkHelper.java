package com.example.np161.mymobileworkerstest.Utils;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkHelper {

    public static boolean isConnectedWifi(Context context){
        ConnectivityManager cm      = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean isWiFi              = false;

        NetworkInfo activeNetwork   = cm.getActiveNetworkInfo();
        boolean isConnected         = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

        if (isConnected){
            isWiFi = activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;
        }

        return isWiFi;
    }
}
