package com.example.np161.mymobileworkerstest.Retrofit;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    private static Retrofit retrofit;

    public static Retrofit getInstance(){
        if(retrofit == null)
        retrofit = new Retrofit.Builder()
                .baseUrl("https://alpha.mymobileworkers.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        return retrofit;
    }

    private RetrofitClient(){

    }
}
