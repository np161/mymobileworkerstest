package com.example.np161.mymobileworkerstest.Entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "photos_lists", foreignKeys = {@ForeignKey(entity = Screen.class, parentColumns = "id", childColumns = "screen_id", onDelete = ForeignKey.CASCADE)})
public class PhotosList {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "photo_type")
    @SerializedName("photo_type")
    @Expose
    private String photoType;

    @ColumnInfo(name = "min_photos_required")
    @SerializedName("min_photos_required")
    @Expose
    private Integer minPhotosRequired;

    @ColumnInfo(name = "annotation_required")
    @SerializedName("annotation_required")
    @Expose
    private Boolean annotationRequired;

    @ColumnInfo(name = "screen_id")
    private int screenId;

    public PhotosList(int id, String photoType, Integer minPhotosRequired, Boolean annotationRequired, int screenId) {
        this.id = id;
        this.photoType = photoType;
        this.minPhotosRequired = minPhotosRequired;
        this.annotationRequired = annotationRequired;
        this.screenId = screenId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhotoType() {
        return photoType;
    }

    public void setPhotoType(String photoType) {
        this.photoType = photoType;
    }

    public Integer getMinPhotosRequired() {
        return minPhotosRequired;
    }

    public void setMinPhotosRequired(Integer minPhotosRequired) {
        this.minPhotosRequired = minPhotosRequired;
    }

    public Boolean getAnnotationRequired() {
        return annotationRequired;
    }

    public void setAnnotationRequired(Boolean annotationRequired) {
        this.annotationRequired = annotationRequired;
    }

    public int getScreenId() {
        return screenId;
    }

    public void setScreenId(int screenId) {
        this.screenId = screenId;
    }
}
