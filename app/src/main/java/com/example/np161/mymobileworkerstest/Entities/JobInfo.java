package com.example.np161.mymobileworkerstest.Entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "job_infos", foreignKeys = {@ForeignKey(entity = Screen.class, parentColumns = "id", childColumns = "screen_id", onDelete = ForeignKey.CASCADE)})
public class JobInfo {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "contact_name")
    @SerializedName("contact_name")
    @Expose
    private String contactName;

    @ColumnInfo(name = "primary_contact_number")
    @SerializedName("primary_contact_number")
    @Expose
    private String primaryContactNumber;

    @ColumnInfo(name = "secondary_contact_number")
    @SerializedName("secondary_contact_number")
    @Expose
    private String secondaryContactNumber;

    @ColumnInfo(name = "address")
    @SerializedName("address")
    @Expose
    private String address;

    @ColumnInfo(name = "postcode")
    @SerializedName("postcode")
    @Expose
    private String postcode;

    @ColumnInfo(name = "special_instructions")
    @SerializedName("special_instructions")
    @Expose
    private String specialInstructions;

    @ColumnInfo(name = "screen_id")
    private int screenId;

    public JobInfo(int id, String contactName, String primaryContactNumber, String secondaryContactNumber, String address, String postcode, String specialInstructions, int screenId) {
        this.id = id;
        this.contactName = contactName;
        this.primaryContactNumber = primaryContactNumber;
        this.secondaryContactNumber = secondaryContactNumber;
        this.address = address;
        this.postcode = postcode;
        this.specialInstructions = specialInstructions;
        this.screenId = screenId;
    }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getPrimaryContactNumber() {
        return primaryContactNumber;
    }

    public void setPrimaryContactNumber(String primaryContactNumber) {
        this.primaryContactNumber = primaryContactNumber;
    }

    public String getSecondaryContactNumber() {
        return secondaryContactNumber;
    }

    public void setSecondaryContactNumber(String secondaryContactNumber) {
        this.secondaryContactNumber = secondaryContactNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getSpecialInstructions() {
        return specialInstructions;
    }

    public void setSpecialInstructions(String specialInstructions) {
        this.specialInstructions = specialInstructions;
    }

    public int getScreenId() {
        return screenId;
    }

    public void setScreenId(int screenId) {
        this.screenId = screenId;
    }
}
