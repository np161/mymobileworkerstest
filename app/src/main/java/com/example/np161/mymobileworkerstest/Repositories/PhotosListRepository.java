package com.example.np161.mymobileworkerstest.Repositories;

import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.np161.mymobileworkerstest.Core.MMWTest;
import com.example.np161.mymobileworkerstest.Database.Daos.PhotosListDao;
import com.example.np161.mymobileworkerstest.Entities.PhotosList;

import java.util.List;

public class PhotosListRepository {

    private PhotosListDao photosListDao;
    private LiveData<List<PhotosList>> photosListList;

    public PhotosListRepository(int screenId){
        photosListDao = MMWTest.getDatabase().photosListDao();
        photosListList = photosListDao.getPhotosListForScreen(screenId);
    }

    public void insert(PhotosList photosList){ new InsertPhotosListTask(photosListDao).execute(photosList); }

    public void update(PhotosList photosList){ new UpdatePhotosListTask(photosListDao).execute(photosList); }

    public void delete(PhotosList photosList){ new DeletePhotosListTask(photosListDao).execute(photosList); }

    public LiveData<List<PhotosList>> getPhotosListList() {
        return photosListList;
    }


    private static class InsertPhotosListTask extends AsyncTask<PhotosList, Void, Void> {
        private PhotosListDao photosListDao;

        private InsertPhotosListTask(PhotosListDao photosListDao){
            this.photosListDao = photosListDao;
        }

        @Override
        protected Void doInBackground(PhotosList... photosLists) {
            photosListDao.insert(photosLists[0]);
            return null;
        }
    }

    private static class UpdatePhotosListTask extends AsyncTask<PhotosList, Void, Void>{
        private PhotosListDao photosListDao;

        private UpdatePhotosListTask(PhotosListDao photosListDao){
            this.photosListDao = photosListDao;
        }

        @Override
        protected Void doInBackground(PhotosList... photosLists) {
            photosListDao.update(photosLists[0]);
            return null;
        }
    }

    private static class DeletePhotosListTask extends AsyncTask<PhotosList, Void, Void>{
        private PhotosListDao photosListDao;

        private DeletePhotosListTask(PhotosListDao photosListDao){
            this.photosListDao = photosListDao;
        }

        @Override
        protected Void doInBackground(PhotosList... photosLists) {
            photosListDao.delete(photosLists[0]);
            return null;
        }
    }
}
