package com.example.np161.mymobileworkerstest.Adpaters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.np161.mymobileworkerstest.Entities.Screen;
import com.example.np161.mymobileworkerstest.JobInfoActivity;
import com.example.np161.mymobileworkerstest.PhotosListActivity;
import com.example.np161.mymobileworkerstest.R;
import com.example.np161.mymobileworkerstest.Core.DialogHelper;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ScreenListAdapter extends RecyclerView.Adapter<ScreenListAdapter.ScreenHolder> {
    private List<Screen> screens = new ArrayList<>();
    private Context context;

    @NonNull
    @Override
    public ScreenListAdapter.ScreenHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_screen, viewGroup, false);
        context = viewGroup.getContext();
        return new ScreenListAdapter.ScreenHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ScreenListAdapter.ScreenHolder screenHolder, int i) {
        final Screen screen = screens.get(i);
        screenHolder.screenBtn.setText( screen.getScreenName() );
        screenHolder.screenBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i;

                switch (screen.getScreenName()){
                    case "Job details":
                        i = new Intent(context, JobInfoActivity.class)
                                .putExtra("screenId", screen.getId());
                        context.startActivity(i);
                        break;
                    case "Job photos":
                        i = new Intent(context, PhotosListActivity.class)
                                .putExtra("screenId", screen.getId());
                        context.startActivity(i);
                        break;
                        default:
                            DialogHelper.showSwalDialog(context, "SCREEN NOT AVAILABLE", "", SweetAlertDialog.ERROR_TYPE);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return screens.size();
    }

    public void setScreens(List<Screen> screens){
        this.screens = screens;
        notifyDataSetChanged();
    }

    class ScreenHolder extends RecyclerView.ViewHolder{
        private Button screenBtn;

        private ScreenHolder(@NonNull View itemView) {
            super(itemView);

            screenBtn = itemView.findViewById(R.id.screen_btn);
        }
    }
}
