package com.example.np161.mymobileworkerstest;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.example.np161.mymobileworkerstest.Adpaters.PhotosListAdapter;
import com.example.np161.mymobileworkerstest.Core.MMWTest;
import com.example.np161.mymobileworkerstest.Entities.PhotosList;
import com.example.np161.mymobileworkerstest.ViewModels.Factories.PhotosListViewModelFactory;
import com.example.np161.mymobileworkerstest.ViewModels.PhotosListViewModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PhotosListActivity extends AppCompatActivity {
    @BindView(R.id.job_number_tview) TextView job_number;
    @BindView(R.id.photos_list_list) RecyclerView photosListList;

    private PhotosListViewModel photosListViewModel;

    private int screenId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photos_list);

        ButterKnife.bind(this);

        screenId = getIntent().getExtras().getInt("screenId");

        job_number.setText(MMWTest.getJob().getJobNumber() + " ");

        photosListList.setLayoutManager(new LinearLayoutManager(this));
        photosListList.setHasFixedSize(true);

        final PhotosListAdapter adapter = new PhotosListAdapter();
        photosListList.setAdapter(adapter);

        photosListViewModel = ViewModelProviders.of(this, new PhotosListViewModelFactory(this.getApplication(), screenId)).get(PhotosListViewModel.class);
        photosListViewModel.getPhotosListList().observe(this, new android.arch.lifecycle.Observer<List<PhotosList>>() {
            @Override
            public void onChanged(@Nullable List<PhotosList> photosLists) {
                adapter.setPhotosLists(photosLists);
            }
        });
    }
}
