package com.example.np161.mymobileworkerstest.ViewModels.Factories;

import android.app.Application;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.example.np161.mymobileworkerstest.ViewModels.JobPhotoViewModel;

public class JobPhotoViewModelFactory implements ViewModelProvider.Factory {
    private Application application;
    private int photosListId;

    public JobPhotoViewModelFactory(Application application, int photosListId) {
        this.application = application;
        this.photosListId = photosListId;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new JobPhotoViewModel(application, photosListId);
    }
}