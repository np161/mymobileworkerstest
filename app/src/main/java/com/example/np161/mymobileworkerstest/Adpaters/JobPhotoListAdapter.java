package com.example.np161.mymobileworkerstest.Adpaters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.np161.mymobileworkerstest.Core.DialogHelper;
import com.example.np161.mymobileworkerstest.Entities.JobPhoto;
import com.example.np161.mymobileworkerstest.R;

import java.util.ArrayList;
import java.util.List;

public class JobPhotoListAdapter extends RecyclerView.Adapter<JobPhotoListAdapter.JobPhotoHolder> {
    private List<JobPhoto> jobPhotos = new ArrayList<>();
    private Context context;

    @NonNull
    @Override
    public JobPhotoListAdapter.JobPhotoHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_job_photo, viewGroup, false);
        context = viewGroup.getContext();
        return new JobPhotoListAdapter.JobPhotoHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull JobPhotoListAdapter.JobPhotoHolder jobPhotoHolder, int i) {
        final JobPhoto jobPhoto = jobPhotos.get(i);

        jobPhotoHolder.latitude.setText(jobPhoto.getLatitude() + "");
        jobPhotoHolder.longitude.setText(jobPhoto.getLongitude() + "");
        jobPhotoHolder.annotation.setText(jobPhoto.getAnnotation() + "");

        jobPhotoHolder.viewPhotoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogHelper.showJobPhotoDialog(context, jobPhoto.getFilepath(), jobPhoto.getAnnotation());
            }
        });
    }

    @Override
    public int getItemCount() {
        return jobPhotos.size();
    }

    public void setJobPhotos(List<JobPhoto> jobPhotos){
        this.jobPhotos = jobPhotos;
        notifyDataSetChanged();
    }

    class JobPhotoHolder extends RecyclerView.ViewHolder{
        private Button viewPhotoBtn;
        private TextView latitude;
        private TextView longitude;
        private TextView annotation;


        private JobPhotoHolder(@NonNull View itemView) {
            super(itemView);

            viewPhotoBtn = itemView.findViewById(R.id.view_photo_btn);
            latitude = itemView.findViewById(R.id.latitude_tview);
            longitude = itemView.findViewById(R.id.longitude_tview);
            annotation = itemView.findViewById(R.id.annotation_tview);
        }
    }
}
