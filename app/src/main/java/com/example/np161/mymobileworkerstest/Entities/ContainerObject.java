package com.example.np161.mymobileworkerstest.Entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ContainerObject {

    @SerializedName("jobs")
    @Expose
    private List<Job> jobs = null;

    public List<Job> getJobs() {
        return jobs;
    }

    public void setJobs(List<Job> jobs) {
        this.jobs = jobs;
    }
}
