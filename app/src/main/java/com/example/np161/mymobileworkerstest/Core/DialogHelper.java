package com.example.np161.mymobileworkerstest.Core;

import android.app.Dialog;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.np161.mymobileworkerstest.R;
import com.squareup.picasso.Picasso;

import java.io.File;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static java.lang.Math.toIntExact;

public class DialogHelper {

    /**
     *
     * @param context
     * @param title
     * @param message
     * @param type
     */
    public static void showSwalDialog(Context context, String title, String message, int type){
        SweetAlertDialog swalDialog = new SweetAlertDialog(context, type)
                .setTitleText(title)
                .setContentText(message);

        swalDialog.show();

        Button btn = swalDialog.findViewById(R.id.confirm_button);
        btn.setBackgroundColor(ContextCompat.getColor(context,R.color.purple));
    }

    /**
     *
     * @param context
     * @param photoId
     */
    public static void showAnnotationDialog(final Context context, final long photoId){
        final Dialog dialog = new Dialog(context, android.R.style.Theme_Material_Light_Dialog_NoActionBar_MinWidth);
        dialog.setContentView(R.layout.dialog_add_annotation);

        final EditText annotation = dialog.findViewById(R.id.annotation_etext);
        Button annotationBtn = dialog.findViewById(R.id.add_annotation_btn);

        annotationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncTaskHelper.saveAnnotation(context, annotation.getText().toString(), toIntExact(photoId)).execute();
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    /**
     *
     * @param context
     * @param filepath
     */
    public static void showJobPhotoDialog(Context context, String filepath, String annotation){
        Dialog dialog = new Dialog(context, android.R.style.Theme_Material_Light_Dialog_NoActionBar_MinWidth);
        dialog.setContentView(R.layout.dialog_show_job_photo);

        ImageView jobPhotoImage = dialog.findViewById(R.id.job_photo_iview);
        TextView annotationTview = dialog.findViewById(R.id.annotation_tview);
        annotationTview.setText(annotation);

        Picasso.with(context).load(new File(filepath)).into(jobPhotoImage);

        dialog.show();
    }

}
