package com.example.np161.mymobileworkerstest.ViewModels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.np161.mymobileworkerstest.Core.MMWTest;
import com.example.np161.mymobileworkerstest.Entities.Screen;
import com.example.np161.mymobileworkerstest.Repositories.ScreenRepository;

import java.util.List;

public class ScreenViewModel extends AndroidViewModel {

    private ScreenRepository screenRepository;
    private LiveData<List<Screen>> screenList;

    public ScreenViewModel(@NonNull Application application) {
        super(application);

        screenRepository = new ScreenRepository(MMWTest.getJob().getId());
        screenList = screenRepository.getScreenList();
    }

    public void insert(Screen screen){
        screenRepository.insert(screen);
    }

    public void update(Screen screen){
        screenRepository.update(screen);
    }

    public void delete(Screen screen){
        screenRepository.delete(screen);
    }

    public LiveData<List<Screen>> getScreenList(){
        return screenList;
    }
}
