package com.example.np161.mymobileworkerstest.ViewModels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.np161.mymobileworkerstest.Entities.JobPhoto;
import com.example.np161.mymobileworkerstest.Repositories.JobPhotoRepository;

import java.util.List;

public class JobPhotoViewModel extends AndroidViewModel {

    private JobPhotoRepository jobPhotoRepository;
    private LiveData<List<JobPhoto>> jobPhotoList;

    public JobPhotoViewModel(@NonNull Application application, int photosListId) {
        super(application);

        jobPhotoRepository = new JobPhotoRepository(photosListId);
        jobPhotoList = jobPhotoRepository.getJobPhotos();
    }

    public void insert(JobPhoto job){
        jobPhotoRepository.insert(job);
    }

    public void update(JobPhoto job){
        jobPhotoRepository.delete(job);
    }

    public LiveData<List<JobPhoto>> getJobPhotoList(){
        return jobPhotoList;
    }
}
