package com.example.np161.mymobileworkerstest.ViewModels.Factories;

import android.app.Application;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.example.np161.mymobileworkerstest.ViewModels.JobInfoViewModel;

public class JobInfoViewModelFactory implements ViewModelProvider.Factory {
    private Application application;
    private int screenId;

    public JobInfoViewModelFactory(Application application, int screenId) {
        this.application = application;
        this.screenId = screenId;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new JobInfoViewModel(application, screenId);
    }
}