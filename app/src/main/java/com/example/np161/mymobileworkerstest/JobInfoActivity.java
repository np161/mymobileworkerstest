package com.example.np161.mymobileworkerstest;

import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.example.np161.mymobileworkerstest.Core.MMWTest;
import com.example.np161.mymobileworkerstest.Entities.JobInfo;
import com.example.np161.mymobileworkerstest.ViewModels.Factories.JobInfoViewModelFactory;
import com.example.np161.mymobileworkerstest.ViewModels.JobInfoViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class JobInfoActivity extends AppCompatActivity {

    @BindView(R.id.job_number_tview) TextView job_number;

    @BindView(R.id.contact_name_etext) EditText contactNameEtext;
    @BindView(R.id.pri_contact_num_etext) EditText priContactNumEtext;
    @BindView(R.id.sec_contact_num_etext) EditText secContactNumEtext;
    @BindView(R.id.address_etext) EditText addressEtext;
    @BindView(R.id.postcode_etext) EditText postcodeEtext;
    @BindView(R.id.special_instructions_etext) EditText specialInstructionsEtext;

    private JobInfoViewModel jobInfoViewModel;

    private int screenId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_info);

        ButterKnife.bind(this);

        screenId = getIntent().getExtras().getInt("screenId");

        job_number.setText(MMWTest.getJob().getJobNumber() + " ");

        jobInfoViewModel = ViewModelProviders.of(this, new JobInfoViewModelFactory(this.getApplication(), screenId)).get(JobInfoViewModel.class);
        jobInfoViewModel.getJobInfo().observe(this, new android.arch.lifecycle.Observer<JobInfo>() {
            @Override
            public void onChanged(@Nullable JobInfo jobInfo) {
                contactNameEtext.setText(jobInfo.getContactName());
                priContactNumEtext.setText(jobInfo.getPrimaryContactNumber());
                secContactNumEtext.setText(jobInfo.getSecondaryContactNumber());
                addressEtext.setText(jobInfo.getAddress());
                postcodeEtext.setText(jobInfo.getPostcode());
                specialInstructionsEtext.setText(jobInfo.getSpecialInstructions());
            }
        });
    }
}
