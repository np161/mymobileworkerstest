package com.example.np161.mymobileworkerstest.Database.Daos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.np161.mymobileworkerstest.Entities.Job;

import java.util.List;

@Dao
public interface JobDao {

    @Query("SELECT * FROM jobs")
    LiveData<List<Job>> getJobList();

    @Insert()
    long insert(Job job);

    @Update()
    void update(Job job);

    @Delete
    void delete(Job job);
}