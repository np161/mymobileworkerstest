package com.example.np161.mymobileworkerstest.Core;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.res.Configuration;

import com.example.np161.mymobileworkerstest.Database.AppDatabase;
import com.example.np161.mymobileworkerstest.Entities.Job;
import com.example.np161.mymobileworkerstest.Retrofit.RetrofitClient;
import com.example.np161.mymobileworkerstest.Retrofit.RetrofitInterface;

public class MMWTest extends Application {

    private static AppDatabase database;
    private static RetrofitInterface retrofit;

    public static AppDatabase getDatabase() {
        return database;
    }

    public static void setDatabase(AppDatabase database) {
        MMWTest.database = database;
    }

    public static RetrofitInterface getRetrofit() {
        return retrofit;
    }

    public static void setRetrofit(RetrofitInterface retrofit) {
        MMWTest.retrofit = retrofit;
    }

    public static Job job;

    public static Job getJob() { return job; }

    public static void setJob(Job job) { MMWTest.job = job; }

    @Override
    public void onCreate() {
        super.onCreate();

//        this.deleteDatabase("database");
        database = Room.databaseBuilder(this, AppDatabase.class, "database").build();
        retrofit = RetrofitClient.getInstance().create(RetrofitInterface.class);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }
}
