package com.example.np161.mymobileworkerstest.Entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "photos", foreignKeys = {@ForeignKey(entity = PhotosList.class, parentColumns = "id", childColumns = "photos_list_id", onDelete = ForeignKey.CASCADE)})
public class JobPhoto {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private String filepath;

    private float latitude;

    private float longitude;

    private String annotation;

    @ColumnInfo(name = "photos_list_id")
    private int photosListId;

    public JobPhoto(int id, String filepath, float latitude, float longitude, String annotation, int photosListId) {
        this.id = id;
        this.filepath = filepath;
        this.latitude = latitude;
        this.longitude = longitude;
        this.annotation = annotation;
        this.photosListId = photosListId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFilepath() {
        return filepath;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public String getAnnotation() {
        return annotation;
    }

    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    public int getPhotosListId() {
        return photosListId;
    }

    public void setPhotosListId(int photosListId) {
        this.photosListId = photosListId;
    }

}
