package com.example.np161.mymobileworkerstest.Core;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;

import com.example.np161.mymobileworkerstest.Entities.ContainerObject;
import com.example.np161.mymobileworkerstest.Entities.Job;
import com.example.np161.mymobileworkerstest.Entities.JobInfo;
import com.example.np161.mymobileworkerstest.Entities.JobPhoto;
import com.example.np161.mymobileworkerstest.Entities.PhotosList;
import com.example.np161.mymobileworkerstest.Entities.Screen;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static java.lang.Math.toIntExact;

public class AsyncTaskHelper {

    public static class saveData extends AsyncTask<ContainerObject, Void, Void> {
        @Override
        protected Void doInBackground(ContainerObject... data) {
            for (Job job : data[0].getJobs()){
                int job_id = toIntExact(MMWTest.getDatabase().jobDao().insert(job));

                for(Screen screen : job.getScreens()){
                    screen.setJobId(job_id);
                    int screen_id = toIntExact(MMWTest.getDatabase().screenDao().insert(screen));

                    JobInfo jobInfo = screen.getJobInfo();
                    List<PhotosList> photosLists = screen.getPhotosList();

                    if(jobInfo != null){
                        jobInfo.setScreenId(screen_id);
                        MMWTest.getDatabase().jobInfoDao().insert(jobInfo);
                    }

                    if (photosLists != null){
                        for (PhotosList photosList : photosLists){
                            photosList.setScreenId(screen_id);
                            MMWTest.getDatabase().photosListDao().insert(photosList);
                        }
                    }
                }
            }

            return null;
        }
    }

    public static class savePhotos extends AsyncTask<Void, Void, Long> {
        private Context context;
        private Uri uri;
        private float latitude;
        private float longitude;
        private int photosListId;

        public savePhotos(Context context, Uri uri, float latitude, float longitude, int photosListId) {
            this.context = context;
            this.uri = uri;
            this.latitude = latitude;
            this.longitude = longitude;
            this.photosListId = photosListId;
        }

        @Override
        protected Long doInBackground(Void... params) {
            JobPhoto jobPhotos = new JobPhoto(0, uri.getPath(), latitude, longitude, "", photosListId);
            return MMWTest.getDatabase().jobPhotoDao().insert(jobPhotos);
        }

        @Override
        protected void onPostExecute(Long result) {
            if (result > 0){
                DialogHelper.showAnnotationDialog(context, result);
            }else{
                DialogHelper.showSwalDialog(context, "IMAGE NOT ADDED", "", SweetAlertDialog.ERROR_TYPE);
            }
        }
    }

    public static class saveAnnotation extends AsyncTask<Void, Void, Boolean> {
        private Context context;
        private String annotation;
        private int photoId;

        public saveAnnotation(Context context, String annotation, int photoId) {
            this.context = context;
            this.annotation = annotation;
            this.photoId = photoId;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            JobPhoto jobPhoto = MMWTest.getDatabase().jobPhotoDao().getJobPhoto(photoId);
            jobPhoto.setAnnotation(annotation);

            MMWTest.getDatabase().jobPhotoDao().update(jobPhoto);
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (result){
                DialogHelper.showSwalDialog(context, "ANNOTATION ADDED", "", SweetAlertDialog.SUCCESS_TYPE);
            }else{
                DialogHelper.showSwalDialog(context, "ANNOTATION NOT ADDED", "", SweetAlertDialog.ERROR_TYPE);
            }
        }
    }
}
